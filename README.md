# nodoBinario
    API para crear árboles binarios.

# Endpoint
    Este api tiene como objetivo la creacion de una arbol binario.
    Nota: Este proyecto esta creado en springboot, por lo cual ya cuenta con un servidor por defecto en cual se puede desplegar con solo abriendolo desde un ide como SpringTools o Intellij IDEA desde la clase *DemoApplication*, posterior a su despliegue se puede acceder a los endpoints desde la url *http://localhost:8080/apiNodo*

    1. Se encuentra el recurso **/apiNodo/init**, el cual inicia la aplicacion y crea un arbol binario de auerdo a una cadena de valores separados por el carater coma (,).
    ejemplo: **_http://localhost:8080/apiNodo/init/67,39,76,28,44,74,85,20,83,87_**

    2. Se encuentra el recuros **apiNodo/find/**, el cual dado una cadena de numeros separados por el carater coma (,) y dos parametros numericos, encontrara el ancestros 
    de los parametros numericos existente en el arbol que se crea de la cadena de numeros.
    ejemplo: http://localhost:8080/apiNodo/find/67,39,76,28,44,74,85,20,83,87/835/874
    
    **NOTA: El recurso numero 2, retorna cero cuando no se encuentra un nodo ancestro.**

# Pruebas Unitarias
    Las clases del paquete "comun" se encuentran totalmente probadas con una cobertura del 100%

# Requerimientos.
    * Java 1.8.
    * SpringBoot 2.5.
    * Gradle 6.8.3.
    * JUnit 4
 
