package com.api.arbol.servicios;

import com.api.arbol.comun.Arbol;
import com.api.arbol.comun.Nodo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class NodoBinarioServicio {

    private Arbol arbol;

    @Autowired
    NodoBinarioServicio(Arbol arbol) {
        this.arbol = arbol;
    }

    public Arbol obtieneArbolServicio() {
        return this.arbol;
    }

    public void iniciarNodo(String listaNumeros) throws Exception {
        Arbol arbol = crearNodoDesdeListaNumero(listaNumeros);
    }

    public int encuentraAncestro(String listaNumeros, int valorPrimerNodo, int valorSegundoNodo) throws Exception {

        try {
            Arbol arbol = crearNodoDesdeListaNumero(listaNumeros);

            Nodo nodoUno = new Nodo(valorPrimerNodo, null, null);
            Nodo nodoDos = new Nodo(valorSegundoNodo, null, null);

            return arbol.buscaAncestroComun(arbol, nodoUno, nodoDos).getValor();
        } catch (NullPointerException x) {
            return 0;
        }
    }

    private Arbol crearNodoDesdeListaNumero(String listaNumeros) throws Exception {

        List<String> valorNodo = Arrays.asList(listaNumeros.split(","));
        for (String valor : valorNodo) {
            this.arbol.crearNodo(Integer.parseInt(valor));
        }
        return this.arbol;
    }
}
