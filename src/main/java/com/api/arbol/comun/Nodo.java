package com.api.arbol.comun;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Nodo {
    private Integer valor;
    private Nodo derecho;
    private Nodo izquierdo;

    public Nodo(Integer valor, Nodo derecho, Nodo izquierdo) {
        this.valor = valor;
        this.derecho = derecho;
        this.izquierdo = izquierdo;
    }
}
