package com.api.arbol.comun;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Setter
@Getter
@Component
public class Arbol {

    private Nodo nodoBase;

    public void crearNodo(Integer valor) throws Exception {
        if (this.nodoBase == null) {
            this.nodoBase = new Nodo(valor, null, null);
        } else {
            agregarNodo(nodoBase, valor);
        }
    }

    private void agregarNodo(Nodo nodo, Integer valor) {
        if (valor > nodo.getValor()) {
            agregarNodoDerecho(nodo, valor);
        } else {
            agregarNodoIzquierdo(nodo, valor);
        }
    }

    private void agregarNodoIzquierdo(Nodo nodo, Integer valor) {
        if (nodo.getIzquierdo() == null) {
            nodo.setIzquierdo(new Nodo(valor, null, null));
        } else {
            agregarNodo(nodo.getIzquierdo(), valor);
        }
    }

    private void agregarNodoDerecho(Nodo nodo, Integer valor) {
        if (nodo.getDerecho() == null) {
            nodo.setDerecho(new Nodo(valor, null, null));
        } else {
            agregarNodo(nodo.getDerecho(), valor);
        }
    }

    public boolean esArbolVacio() {
        return (getNodoBase() != null);
    }

    public boolean esTieneHojasVacias(Arbol arbol) throws Exception {
        return (arbol.getNodoBase().getDerecho() == null && arbol.getNodoBase().getIzquierdo() == null);
    }

    public Nodo buscaAncestroComun(Arbol arbol, Nodo nodoUno, Nodo nodoDos) {
        if (nodoUno.getValor().equals(nodoDos.getValor())) {
            return nodoDos;
        }
        do {
            if (nodoUno.getValor() < arbol.getNodoBase().getValor() && nodoDos.getValor() < arbol.getNodoBase().getValor()) {
                arbol.setNodoBase(arbol.getNodoBase().getIzquierdo());
            } else if (nodoUno.getValor() > arbol.getNodoBase().getValor() && nodoDos.getValor() > arbol.getNodoBase().getValor()) {
                arbol.setNodoBase(arbol.getNodoBase().getDerecho());
            } else {
                return arbol.getNodoBase();
            }
        } while (true);
    }
}
