package com.api.arbol.controles;

import com.api.arbol.servicios.NodoBinarioServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/apiNodo")
public class NodoControlador {
    @Autowired
    private NodoBinarioServicio nodoBinarioServicio;

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping(value = "/init/{listaNumeros")
    public void creaNodo(@PathVariable(name = "listaNumeros") String listaNumeros) throws Exception {
        nodoBinarioServicio.iniciarNodo(listaNumeros);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping(value = "find/{listaNumeros}/{valorNodoUno}/{valorNodoDos}")
    public int encuentraAncestroComun(@PathVariable(name = "listaNumeros") String listaNumeros,
                                      @PathVariable(name = "valorNodoUno") int valorNodoUno,
                                      @PathVariable(name = "valorNodoDos") int valorNodoDos) throws Exception {

        return nodoBinarioServicio.encuentraAncestro(listaNumeros, valorNodoUno, valorNodoDos);
    }
}
