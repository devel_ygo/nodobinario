package com.api.arbol.builder;

import com.api.arbol.comun.Nodo;

public class NodoBuilderTest {
    private Integer valor;
    private Nodo derecho;
    private Nodo izquierdo;

    public NodoBuilderTest(){
        this.valor = 1;
        this.derecho = null;
        this.izquierdo = null;
    }

    public NodoBuilderTest conValor(Integer valor){
        this.valor = valor;
        return this;
    }

    public NodoBuilderTest conDerecho(Nodo derecho){
        this.derecho = derecho;
        return this;
    }

    public NodoBuilderTest conIzquierdo(Nodo izquierdo){
        this.izquierdo = izquierdo;
        return this;
    }

    public Nodo build() {
        return new Nodo(valor,derecho,izquierdo);
    }
}
