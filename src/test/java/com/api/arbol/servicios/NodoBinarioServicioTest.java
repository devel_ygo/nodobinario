package com.api.arbol.servicios;

import com.api.arbol.comun.Arbol;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class NodoBinarioServicioTest {
    private Arbol arbol;
    private NodoBinarioServicio nodoBinarioServicio;

    private static final String LISTA_NUMEROS = "67,39,76,28,44,74,85,20,83,87";

    @BeforeEach
    void setUp() {
        arbol = new Arbol();
        nodoBinarioServicio = new NodoBinarioServicio(arbol);
    }

    @Test
    public void inicarNodoTest() throws Exception {
        nodoBinarioServicio.iniciarNodo(LISTA_NUMEROS);
        assertNotNull(nodoBinarioServicio.obtieneArbolServicio());
    }

    @ParameterizedTest
    @CsvSource({"29,44,39", "44,85,67", "83,87,85", "3,3,3","835,874,0"})
    public void encuentraAncestroTest(int valorNodoUno, int valorNodoDos, int valorAncestroComun) throws Exception {
        int actual = nodoBinarioServicio.encuentraAncestro(LISTA_NUMEROS, valorNodoUno, valorNodoDos);
        assertNotNull(actual);
        assertEquals(valorAncestroComun, actual);

    }

    @AfterEach
    void tearDown() {
        arbol = null;
        nodoBinarioServicio = null;
    }
}