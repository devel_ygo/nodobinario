package com.api.arbol.comun;

import com.api.arbol.builder.NodoBuilderTest;
import com.api.arbol.constantes.ConstantesNumeros;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class ArbolTest {

    private static final Integer VALOR_RAIZ_NODO = ConstantesNumeros.NUEVE;
    private Arbol arbol, arbolBusquedad;
    private Nodo nodo;

    @BeforeEach
    void setUp() throws Exception {
        arbol = new Arbol();
        nodo = new NodoBuilderTest().build();
        arbolBusquedad = crearArbol();
    }

    private Arbol crearArbol() throws Exception {
        Arbol arbol = new Arbol();
        arbol.crearNodo(67);

        arbol.crearNodo(39);
        arbol.crearNodo(76);

        arbol.crearNodo(28);
        arbol.crearNodo(44);
        arbol.crearNodo(74);
        arbol.crearNodo(85);

        arbol.crearNodo(20);
        arbol.crearNodo(83);
        arbol.crearNodo(87);

        return arbol;
    }

    @Test
    public void crearNodoCuandoEsNuloTest() throws Exception {
        //Arrange
        arbol.setNodoBase(null);
        //Act
        arbol.crearNodo(VALOR_RAIZ_NODO);
        //Assert
        assertNotNull(arbol.getNodoBase());
    }

    @Test
    public void crearNodoCuandoNoEsNuloTest() throws Exception {
        //Arrange
        arbol.setNodoBase(new NodoBuilderTest().conValor(ConstantesNumeros.UNO).build());
        //Act
        arbol.crearNodo(VALOR_RAIZ_NODO);
        //Assert
        assertNotNull(arbol.getNodoBase());
    }

    @Test
    public void cuandoAgragarNodoOkTest() throws Exception {
        //Arrange
        nodo = new NodoBuilderTest().conValor(ConstantesNumeros.DIEZ)
                .build();
        arbol.setNodoBase(nodo);
        //Act
        arbol.crearNodo(VALOR_RAIZ_NODO);
        //Assert
        assertEquals(arbol.getNodoBase().getValor(), ConstantesNumeros.DIEZ);
    }

    @Test
    public void cuandoAgregarNodoDerechoOkTest() throws Exception {
        //Arrange
        nodo = new NodoBuilderTest().conValor(ConstantesNumeros.CINCO)
                .conDerecho(new NodoBuilderTest().conValor(ConstantesNumeros.CUATRO).build())
                .build();
        arbol.setNodoBase(nodo);
        //Act
        arbol.crearNodo(VALOR_RAIZ_NODO);
        //Assert
        assertEquals(ConstantesNumeros.CINCO, arbol.getNodoBase().getValor());
        assertEquals(ConstantesNumeros.CUATRO, arbol.getNodoBase().getDerecho().getValor());
        assertNull(arbol.getNodoBase().getIzquierdo());
    }

    @Test
    public void cuandoAgregarNodoIzquierdaOkTest() throws Exception {
        //Arrange
        nodo = new NodoBuilderTest().conValor(ConstantesNumeros.DIEZ)
                .conIzquierdo(new NodoBuilderTest().conValor(ConstantesNumeros.ONCE).build())
                .build();
        arbol.setNodoBase(nodo);
        //Act
        arbol.crearNodo(VALOR_RAIZ_NODO);
        //Assert
        assertEquals(ConstantesNumeros.DIEZ, arbol.getNodoBase().getValor());
        assertEquals(ConstantesNumeros.ONCE, arbol.getNodoBase().getIzquierdo().getValor());
        assertNull(arbol.getNodoBase().getDerecho());
    }


    @ParameterizedTest
    @CsvSource({"29,44,39", "44,85,67", "83,87,85", "3,3,3"})
    public void buscaAncestroComun(int valorNodoUno, int valorNodoDos, int valorAncestroComun) throws Exception {
        Nodo nodoUno, nodoDos, nodoRetorno;
        nodoUno = new NodoBuilderTest().conValor(valorNodoUno).build();
        nodoDos = new NodoBuilderTest().conValor(valorNodoDos).build();

        nodoRetorno = arbol.buscaAncestroComun(arbolBusquedad, nodoUno, nodoDos);

        assertNotNull(nodoRetorno);
        assertEquals(valorAncestroComun, nodoRetorno.getValor());

    }

    @Test
    public void cuandoNodoNoEstaVacio() throws Exception {
        arbol.crearNodo(ConstantesNumeros.SEIS);
        assertTrue(arbol.esArbolVacio());
    }

    @Test
    public void cuandoNodoTieneladosVacio() throws Exception {

        arbol.setNodoBase(new NodoBuilderTest().conValor(ConstantesNumeros.SEIS)
                .conDerecho(null)
                .conIzquierdo(null)
                .build());

        boolean esArbolVacio = arbol.esTieneHojasVacias(arbol);
        assertTrue(esArbolVacio);
    }

    @AfterEach
    void tearDown() {
        arbol = null;
        arbolBusquedad = null;
        nodo = null;
    }
}