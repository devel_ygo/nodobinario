package com.api.arbol.comun;

import com.api.arbol.builder.NodoBuilderTest;
import com.api.arbol.constantes.ConstantesNumeros;
import junitparams.JUnitParamsRunner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(JUnitParamsRunner.class)
class NodoTest {

    private Nodo nodo;
    @BeforeEach
    void setUp() {
        nodo = new NodoBuilderTest().build();
    }

    @Test
    public void cuandoCrearNodoOkTest (){
        nodo = new NodoBuilderTest().conValor(ConstantesNumeros.DOS).build();
        assertNotNull(nodo);
    }

    @AfterEach
    void tearDown() {
        nodo = null;
    }
}